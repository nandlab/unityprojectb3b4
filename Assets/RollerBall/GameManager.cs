using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace RollerBall
{
public class GameManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI playtimeTextMesh;
    [SerializeField] TextMeshProUGUI recordTextMesh;
    float startTime;
    float record;
    const float NO_RECORD = -1;
    const string RECORD_KEY = "record";

    public static GameManager instance {
        get;
        private set;
    }

    void Awake()
    {
        instance = this;
    }

    void ShowUI()
    {
        float playtime = Time.time - startTime;
        playtimeTextMesh.text = $"{playtime:0.00}";
        recordTextMesh.text = "Record: ";
        recordTextMesh.text += record == NO_RECORD ? "-" : $"{record:0.00}";
    }

    void Start()
    {
        startTime = Time.time;
        record = PlayerPrefs.GetFloat(RECORD_KEY, NO_RECORD);
        ShowUI();
    }

    void Update()
    {
        ShowUI();
    }

    void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Win()
    {
        Debug.Log("You win!");
        float playtime = Time.time - startTime;
        if (record == NO_RECORD || playtime < record)
        {
            Debug.Log("New record!");
            record = playtime;
            PlayerPrefs.SetFloat(RECORD_KEY, record);
        }
        ReloadScene();
    }


    public void Loose()
    {
        Debug.Log("Try again!");
        ReloadScene();
    }
}
}
