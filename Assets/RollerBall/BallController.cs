using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace RollerBall
{
public class BallController : MonoBehaviour
{
    Rigidbody rb;
    int turn = 0;
    float lateralVelocity = 0;
    bool jump = false;
    bool dash = false;
    bool onGround = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void AdjustCameraPosition()
    {
        Camera.main.transform.position = new Vector3(rb.position.x, rb.position.y + 3.5f, rb.position.z - 8);
    }

    void Start()
    {
        AdjustCameraPosition();
    }

    void FixedUpdate()
    {
        lateralVelocity += 8 * turn * Time.deltaTime;
        Vector3 vel = rb.velocity;
        vel.z = dash ? 14 : 7;
        vel.x = lateralVelocity;
        if (jump)
        {
            jump = false;
            vel.y += 5.0f;
        }
        rb.velocity = vel;
    }

    void Update()
    {
        AdjustCameraPosition();
        if (rb.position.y < 0) {
            GameManager.instance.Loose();
        }
    }

    public void Move(InputAction.CallbackContext callbackContext)
    {
        Vector2 vect = callbackContext.ReadValue<Vector2>();
        dash = vect.y > 0;
        if (vect.x > 0)
            turn = 1;
        else if (vect.x == 0)
            turn = 0;
        else
            turn = -1;
    }

    public void Exit(InputAction.CallbackContext callbackContext)
    {
        SceneManager.LoadScene("RollerBallSelection");
    }

    void OnCollisionEnter(Collision collision)
    {
        onGround = true;
    }

    void OnCollisionExit(Collision collision)
    {
        onGround = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            GameManager.instance.Win();
        }
    }

    public void Jump(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.started && onGround)
        {
            jump = true;
        }
    }
}
}
