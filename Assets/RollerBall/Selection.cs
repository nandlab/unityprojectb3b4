using UnityEngine;
using UnityEngine.SceneManagement;

namespace RollerBall
{
public class Selection : MonoBehaviour
{
    public void StartLevel()
    {
        SceneManager.LoadScene("RollerBall");
    }
}
}
