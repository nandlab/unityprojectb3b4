using UnityEngine;
using UnityEngine.SceneManagement;

namespace OpenWorld
{
public class WorldSelection : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void StartLevel()
    {
        SceneManager.LoadScene("OpenWorld");
    }
}
}
