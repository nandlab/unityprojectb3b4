using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenWorld
{
public class DayNightCycle : MonoBehaviour
{
    float angle = 90.0f;

    void Start()
    {
        transform.localRotation = Quaternion.Euler(angle, 0, 0);
    }

    void Update()
    {
        angle += Time.deltaTime;
        transform.localRotation = Quaternion.Euler(angle, 0, 0);
    }
}
}
