using UnityEngine;

namespace OpenWorld
{
public class FlashLight : MonoBehaviour
{
    [SerializeField] Light m_FlashLight;
    private AudioSource flashlightToggleSound;

    void Awake()
    {
        flashlightToggleSound = GetComponents<AudioSource>()[1];
    }

    void OnFlashLight()
    {
        Debug.Log("Toggle flash light.");
        m_FlashLight.enabled = !m_FlashLight.enabled;
        flashlightToggleSound.Play();
    }
}
}
