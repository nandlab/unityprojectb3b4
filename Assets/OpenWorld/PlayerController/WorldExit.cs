using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldExit : MonoBehaviour
{
    void OnExit()
    {
        SceneManager.LoadScene("OpenWorldSelection");
    }
}
