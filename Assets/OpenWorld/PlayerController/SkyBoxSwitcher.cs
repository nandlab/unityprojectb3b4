using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OpenWorld
{
public class SkyBoxSwitcher : MonoBehaviour
{
    [SerializeField] Material[] m_SkyboxMaterials;
    int nextIndex = 0;

    void Awake()
    {
        Debug.Assert(m_SkyboxMaterials.Length > 0);
    }

    void OnSwitchSkybox()
    {
        Debug.Log("Switch skybox.");
        RenderSettings.skybox = m_SkyboxMaterials[nextIndex];
        nextIndex = (nextIndex + 1) % m_SkyboxMaterials.Length;
    }
}
}
