using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    void OnChooseRollerBall()
    {
        SceneManager.LoadScene("RollerBallSelection");
    }

    void OnChooseOpenWorld()
    {
        SceneManager.LoadScene("OpenWorldSelection");        
    }
}
